﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayReverse
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] examArray = { 11, 56, 77, 88, 55, 44, 22, 65, 88 };

            foreach (var item in ReverseArray(examArray))
            {
                Console.WriteLine(item);            
            }
            Console.ReadKey();
        }

        public static int[] ReverseArray(int[] value)
        {
            value = CalculateReverse(value);

            return value;
        }

        public static int[] CalculateReverse(int[] value)
        {
            int temporaryValue;
            int roundValue = 2;


            for (int i = 0; i < value.Length / roundValue; i++)
            {
                temporaryValue = value[i];
                value[i] = value[value.Length - i - 1];
                value[value.Length - i - 1] = temporaryValue;
            }

            return value;
        }
    }
}
